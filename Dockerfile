FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y curl git clang-format docker-compose protobuf-compiler python3-dev python3-pip gcc --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install poetry==1.1.6  # Current version from fedora:34

